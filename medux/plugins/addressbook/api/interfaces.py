from gdaps.api import Interface

# You can place your interfaces here, to be included from dependent plugins

# from gdaps import Interface
# @Interface
# class IMySpecialAddressbookInterface:
#     """Please provide a detailed documentation of the interface."""
#
#     def do_something(self):
#         """Documentation of method"""
#
#
#     Then implement this interface in another, dependent plugin:
#
#     from .api.interfaces import IMySpecialAddressbookInterface
#
#     class OtherPluginClass(IMySpecialAddressbookInterface):
#
#        def do_something(self):
#            """do something."""
#
